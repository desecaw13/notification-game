package net.lil.notificationgame

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationManagerCompat

class GameService : Service() {

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val choiceID = intent?.getIntExtra(CHOICE_ID, 0)
        if (choiceID == null) {
            Log.e("GameService", "intent was null in GameService.onStartCommand")
            return super.onStartCommand(intent, flags, startId)
        }

        game.builder.clearActions()

        game.updateNotification(applicationContext, choiceID)

        with(NotificationManagerCompat.from(applicationContext)) {
            notify(0, game.builder.build())
        }

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }
}
