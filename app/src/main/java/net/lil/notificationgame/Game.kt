package net.lil.notificationgame

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat

class Game {
    // the builder for the notification
    lateinit var builder: NotificationCompat.Builder

    // sets up service intents for the game
    fun createIntent(context: Context, choiceID: Int): PendingIntent {
        val actionIntent = Intent(context, GameService::class.java).apply {
            putExtra(CHOICE_ID, choiceID)
        }
        return PendingIntent.getService(
            context,
            choiceID,
            actionIntent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    // the game logic
    fun updateNotification(context: Context, choiceID: Int) {
        when (choiceID) {
            0 -> { // intro
                builder.setContentTitle(context.getString(R.string.intro_title))
                    .setContentText(context.getString(R.string.intro_content))
                    .setStyle(
                        NotificationCompat.BigTextStyle()
                            .bigText(context.getString(R.string.intro_content))
                    ).addAction(
                        R.drawable.ic_launcher_foreground,
                        context.getString(R.string.intro_continue),
                        game.createIntent(context, 1)
                    )
            }
            1 -> { // start
                builder.setContentTitle(context.getString(R.string.start_title))
                    .setContentText(context.getString(R.string.start_content))
                    .setStyle(
                        NotificationCompat.BigTextStyle()
                            .bigText(context.getString(R.string.start_content))
                    ).addAction(
                        R.drawable.ic_launcher_foreground,
                        context.getString(R.string.start_attack),
                        game.createIntent(context, 2)
                    ).addAction(
                        R.drawable.ic_launcher_foreground,
                        context.getString(R.string.start_talk),
                        game.createIntent(context, 3)
                    )
            }
            2 -> { // attack
                builder.setContentTitle(context.getString(R.string.attack_title))
                    .setContentText(context.getString(R.string.attack_content))
                    .setStyle(
                        NotificationCompat.BigTextStyle()
                            .bigText(context.getString(R.string.attack_content))
                    ).addAction(
                        R.drawable.ic_launcher_foreground,
                        context.getString(R.string.attack_keep),
                        game.createIntent(context, 5)
                    ).addAction(
                        R.drawable.ic_launcher_foreground,
                        context.getString(R.string.attack_run),
                        game.createIntent(context, 4)
                    )
            }
            3 -> { // talk
                builder.setContentTitle(context.getString(R.string.talk_title))
                    .setContentText(context.getString(R.string.talk_content))
                    .setStyle(
                        NotificationCompat.BigTextStyle()
                            .bigText(context.getString(R.string.talk_content))
                    ).addAction(
                        R.drawable.ic_launcher_foreground,
                        context.getString(R.string.talk_run),
                        game.createIntent(context, 4)
                    )
            }
            4 -> { // run
                builder.setContentTitle(context.getString(R.string.run_title))
                    .setContentText(context.getString(R.string.run_content))
                    .setStyle(
                        NotificationCompat.BigTextStyle()
                            .bigText(context.getString(R.string.run_content))
                    ).addAction(
                        R.drawable.ic_launcher_foreground,
                        context.getString(R.string.run_look),
                        game.createIntent(context, 6)
                    )
            }
            5 -> { // death (from attacking again)
                builder.setContentTitle(context.getString(R.string.death_title))
                    .setContentText(context.getString(R.string.death_content))
                    .setStyle(
                        NotificationCompat.BigTextStyle()
                            .bigText(context.getString(R.string.death_content))
                    ).addAction(
                        R.drawable.ic_launcher_foreground,
                        context.getString(R.string.death_retry),
                        game.createIntent(context, 1)
                    )
            }
            6 -> { // looking
                builder.setContentTitle(context.getString(R.string.look_title))
                    .setContentText(context.getString(R.string.look_content))
                    .setStyle(
                        NotificationCompat.BigTextStyle()
                            .bigText(context.getString(R.string.look_content))
                    ).addAction(
                        R.drawable.ic_launcher_foreground,
                        context.getString(R.string.look_forward),
                        game.createIntent(context, 7)
                    ).addAction(
                        R.drawable.ic_launcher_foreground,
                        context.getString(R.string.look_left),
                        game.createIntent(context, 7)
                    ).addAction(
                        R.drawable.ic_launcher_foreground,
                        context.getString(R.string.look_right),
                        game.createIntent(context, 7)
                    )
            }
            7 -> { // unofficial end
                builder.setContentTitle(context.getString(R.string.un_end_title))
                    .setContentText(context.getString(R.string.un_end_content))
                    .setStyle(
                        NotificationCompat.BigTextStyle()
                            .bigText(context.getString(R.string.un_end_content))
                    ).addAction(
                        R.drawable.ic_launcher_foreground,
                        context.getString(R.string.un_end_restart),
                        game.createIntent(context, 0)
                    )
            }

            // todo: function that does this copy-paste for me
            /*
            N -> {
                builder.setContentTitle(context.getString(R.string.NNNN_title))
                    .setContentText(context.getString(R.string.NNNN_content))
                    .setStyle(
                        NotificationCompat.BigTextStyle()
                            .bigText(context.getString(R.string.NNNN_content))
                    ).addAction(
                        R.drawable.ic_launcher_foreground,
                        context.getString(R.string.NNNN_option1),
                        game.createIntent(context, N1)
                    ).addAction(
                        R.drawable.ic_launcher_foreground,
                        context.getString(R.string.NNNN_option2),
                        game.createIntent(context, N2)
                    )
            }
             */
        }
    }
}

val game = Game() // i prommy wouldn't do this if u were paying me
